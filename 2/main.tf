provider "aws" {
  access_key = "****************"  //Change credentials
  secret_key = "********************" //Change credentials
  region = "eu-west-1"
  version = "~> 2.70"
}

resource "aws_vpc" "dof-vpc" {
  cidr_block = var.v-cidr-block-vpc
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
      Name = "DOF-VPC"
  }
}

resource "aws_internet_gateway" "dof-igw" {
    vpc_id = aws_vpc.dof-vpc.id 
    tags = {
        Name = "DOF-IGW"
    }
}

resource "aws_route_table" "dof-prt" {
    vpc_id = aws_vpc.dof-vpc.id
    route {
        cidr_block = var.v-cidr-block-rt
        gateway_id = aws_internet_gateway.dof-igw.id
    }
    tags = {
        Name = "DOF-PUBLIC_RT"
    }
}

resource "aws_subnet" "dof-snet" {
    count = var.v-count
    vpc_id = aws_vpc.dof-vpc.id
    cidr_block = var.dof-cidr[count.index]
    map_public_ip_on_launch = true
    availability_zone = data.aws_availability_zones.dof-avz.names[count.index]
    tags = {
        Name = "DOF-SUB-NET-${count.index + 1}"
    }
}

resource "aws_route_table_association" "dof-prt-assoc" {
    count = var.v-count
    subnet_id = aws_subnet.dof-snet.*.id[count.index]
    route_table_id = aws_route_table.dof-prt.id
}

resource "aws_security_group" "dof-pub-sg" {
    name = "dof-pub-sg"
    description = "SOF Public SG"
    vpc_id = aws_vpc.dof-vpc.id
    ingress {
        from_port = 22
        to_port = 22
        protocol =  var.v-protocol-ingress
        cidr_blocks = var.v-cidr-blocks
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = var.v-protocol-ingress
        cidr_blocks = var.v-cidr-blocks
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = var.v-protocol-egress
        cidr_blocks = var.v-cidr-blocks
    }
}

resource "aws_instance" "dof-server" {
    count = var.v-count
    ami = var.v-ami-image
    instance_type = var.v-instance-type
    key_name = var.v-instance-key
    vpc_security_group_ids = [aws_security_group.dof-pub-sg.id]
    subnet_id = element(aws_subnet.dof-snet.*.id, count.index)
    tags = {
        Name = "dof-server-${count.index + 1}"
    }

    provisioner "file" {
        source      = var.v-aws-source
        destination = var.v-aws-destination
        connection {
            type = var.v-connection-type
            user = var.v-ec2-user
            private_key = file(var.v-file-pem)
            host = self.public_ip
        }
    }
    provisioner "remote-exec" {
        inline = var.v-aws-inline-command
        connection {
            type = var.v-connection-type
            user = var.v-ec2-user
            host = self.public_ip
            private_key = file(var.v-file-pem)
        }
    }
}