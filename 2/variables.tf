variable "v-aws-destination" {
    description = "AWS instance destionation folder"
    default = "/tmp/provision.sh"
}

variable "v-aws-source" {
    description = "AWS instance source folder"
    default = "./provision.sh"
}

variable "v-aws-inline-command" {
    description = "AWS instance incline commands"
    default = ["chmod +x /tmp/provision.sh","/tmp/provision.sh"]
}

variable "v-region" {
    description = "Region"
    default = "eu-west-1"
}
variable "v-cidr-block-vpc" {
    description = "Cidr block VPC"
    default = "10.10.0.0/16"
}
variable "v-cidr-block-rt" {
    description = "Cidr block Route table"
    default = "0.0.0.0/0"
}
variable "v-cidr-blocks" {
    description = "Cidr blocks"
    default = ["0.0.0.0/0"]
}
variable "v-file-pem" {
    description = "Pair keys file"
    default = "keypath.pem"
}
variable "v-connection-type" {
    description = "SSH connection type"
    default = "ssh"
}
variable "v-ec2-user" {
    description = "AWS instance user"
    default = "ec2-user"
}
variable "v-protocol-ingress" {
    description = "Protocol for ingress"
    default = "tcp"
}
variable "v-protocol-egress" {
    description = "Protocol for egress"
    default = "-1"
}
variable "v-ami-image" {
    description = "AMI image"
    default = "ami-0ab507a86aac96b91"
}

variable "v-instance-type" {
    description = "EC2 instance type"
    default = "t2.micro"
}

variable "v-instance-key" {
    description = "Instance key"
    default = "terraform"
}

variable "v-count" {
    description = "Resource count"
    default = "2"
}

data "aws_availability_zones" "dof-avz" {}
variable "dof-cidr" {
    type = list
    default = ["10.10.10.0/24", "10.10.11.0/24"] 
}