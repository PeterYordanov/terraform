#!/bin/bash
sudo yum install -y epel-release
sudo zypper install docker
sudo systemctl enable docker.service
sudo cp CA /etc/pki/trust/anchors/ && update-ca-certificates
sudo systemctl start docker.service
sudo /usr/sbin/usermod -aG docker ec2-user
sudo docker pull shekeriev/dob-w3-php
sudo docker pull shekeriev/dob-w3-mysql
sudo docker run --name dob-w3-mysql -e MYSQL_ROOT_PASSWORD=12345 -d shekeriev/dob-w3-mysql
sudo docker run -p 8080:80 -d shekeriev/dob-w3-php
