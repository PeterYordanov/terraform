output "public_ip" {
  description = "Public IP"
  value = aws_instance.dof-server.*.public_ip
}
