echo "* Add hosts ..."
echo "192.168.99.100 docker.dof.lab docker" >> /etc/hosts

echo "* Add Docker repository ..."
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

echo "* Add the missing dependency ..."
dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.1.el7.x86_64.rpm

echo "* Install Docker ..."
dnf install -y docker-ce docker-ce-cli

echo "* Adjust Docker configuration ..."
mkdir -p /etc/docker
echo '{ "hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"] }' | tee /etc/docker/daemon.json
mkdir -p /etc/systemd/system/docker.service.d/
echo [Service] | tee /etc/systemd/system/docker.service.d/docker.conf
echo ExecStart= | tee -a /etc/systemd/system/docker.service.d/docker.conf
echo ExecStart=/usr/bin/dockerd | tee -a /etc/systemd/system/docker.service.d/docker.conf

echo "* Enable and start Docker ..."
systemctl enable docker
systemctl start docker

echo "* Firewall - open port 80, 2375, and 8080 ..."
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=2375/tcp --permanent
firewall-cmd --add-port=8080/tcp --permanent

echo "* Firewall - set adapter zone ..."
firewall-cmd --add-interface docker0 --zone trusted --permanent

echo "* Firewall - reload rules ..."
firewall-cmd --reload

echo "* Add vagrant user to docker group ..."
usermod -aG docker vagrant

# The below lines should be uncommented after the docker images are pulled and running
echo "* Copy the website files from machine to php container ..."
cd /
docker cp var/www/html/. web-php:var/www/html

