variable "image-web" {
    default = "shekeriev/dob-w3-php"
}

variable "image-db" {
    default = "shekeriev/dob-w3-mysql"
}

variable "web-name" {
    default = "web-php" 
}

variable "db-name" {
    default = "db-mysql" 
}

variable "db-env" {
    default = ["MYSQL_ROOT_PASSWORD=12345"]
}