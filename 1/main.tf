provider "docker" {
    host = "tcp://192.168.99.100:2375/"
    version = "~> 2.7"
}

resource "docker_image" "image-web" {
    name = var.image-web
}

resource "docker_image" "image-db" {
    name = var.image-db
}

resource "docker_container" "container-web" {
    name = var.web-name
    image = docker_image.image-web.latest
    ports {
        internal = 80
        external = 80
    }
}

resource "docker_container" "container-db" {
    name = var.db-name
    image = docker_image.image-db.latest
    env = var.db-env
    ports {
        internal = 3306
        external = 3306
    }
}