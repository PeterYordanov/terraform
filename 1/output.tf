output "image-id-web" {
  description = "Image ID"
  value = docker_image.image-web.id
  sensitive = false
}

output "image-name-web" {
  description = "Image Name"
  value = docker_image.image-web.name
  sensitive = false
}

output "image-id-db" {
  description = "Image ID"
  value = docker_image.image-db.id
  sensitive = false
}

output "image-name-db" {
  description = "Image Name"
  value = docker_image.image-db.name
  sensitive = false
}

output "container-id-web" {
  description = "Container ID"
  value = docker_container.container-web.id
  sensitive = false
}

output "container-name-web" {
  description = "Container Name"
  value = docker_container.container-web.name
  sensitive = false
}

output "container-id-db" {
  description = "Container ID"
  value = docker_container.container-db.id
  sensitive = false
}

output "container-name-db" {
  description = "Container Name"
  value = docker_container.container-db.name
  sensitive = false
}

output "container-password-db" {
  description = "DB Password"
  value = docker_container.container-db.env
  sensitive = true
}